resource "aws_s3_bucket" "sample-bucket" {
  bucket = "${var.bucketname}"
  acl    = "private"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}